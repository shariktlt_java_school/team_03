package ru.edu.project.backend.da.jpa;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.edu.project.backend.api.group.Group;
import ru.edu.project.backend.api.students.Student;
import ru.edu.project.backend.app.DemoBackendApplication;
import ru.edu.project.backend.converter.StudentConverter;
import ru.edu.project.backend.da.jpa.entity.GroupEntity;
import ru.edu.project.backend.da.jpa.entity.StudentEntity;
import ru.edu.project.backend.da.jpa.repository.StudentEntityRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

public class JPAStudentDATest {

    @Mock
    private JPAStudentDA jpaStudentDA;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void getAllStudents() {
        Student student = Student.builder().build();
        when(jpaStudentDA.getAllStudents()).thenReturn(Arrays.asList(student));

        List<Student> allStudents = jpaStudentDA.getAllStudents();
        assertEquals(1,allStudents.size());
        verify(jpaStudentDA,timeout(1)).getAllStudents();
    }

    @Test
    public void saveStudent() {
        Student student = Student.builder().studentId(1L).firstName("name").lastName("last").build();
        when(jpaStudentDA.saveStudent(any(Student.class))).thenReturn(student);

        Student actual = jpaStudentDA.saveStudent(student);
        assertEquals(student,actual);
        verify(jpaStudentDA,timeout(1)).saveStudent(any(Student.class));
    }

    @Test
    public void getStudentById() {
        Student student = Student.builder().studentId(1L).firstName("name").lastName("last").build();
        when(jpaStudentDA.getStudentById(anyLong())).thenReturn(student);

        Student studentById = jpaStudentDA.getStudentById(anyLong());
        assertEquals(student,studentById);
        verify(jpaStudentDA,timeout(1)).getStudentById(anyLong());
    }

    @Test
    public void deleteStudentById() {
        long id = 1;
        doNothing().when(jpaStudentDA).deleteStudentById(isA(Long.class));
        jpaStudentDA.deleteStudentById(id);

        verify(jpaStudentDA, timeout(1)).deleteStudentById(id);
    }
}