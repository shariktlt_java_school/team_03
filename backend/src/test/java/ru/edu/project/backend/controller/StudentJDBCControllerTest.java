package ru.edu.project.backend.controller;

import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.edu.project.backend.api.students.Student;
import ru.edu.project.backend.da.StudentDALayer;
import ru.edu.project.backend.service.StudentServiceLayer;

import java.util.Arrays;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(StudentJDBCController.class)
public class StudentJDBCControllerTest {

    @Autowired
    private MockMvc mvc;

    @InjectMocks
    private StudentJDBCController controller;

    @Mock
    private StudentServiceLayer delegate;

    private Student student;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
        mvc = MockMvcBuilders.standaloneSetup(controller).build();
        student = Student.builder().studentId(5L).firstName("first name").build();
    }

    @Test
    @SneakyThrows
    public void getAllStudents() {
        given(delegate.getAllStudents()).willReturn(Arrays.asList(student));
        mvc.perform(get("/user/getAllStudents")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void saveStudent() {
        when(delegate.saveStudent(any(Student.class))).thenReturn(any(Student.class));
        String json = "{}";
        RequestBuilder builder = MockMvcRequestBuilders
                .post("/user/saveStudent")
                .accept(MediaType.APPLICATION_JSON)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mvc.perform(builder).andReturn();
        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
    }

    @Test
    @SneakyThrows
    public void getStudentById() {
        given(delegate.getStudentById(any(Long.class))).willReturn(any(Student.class));

        mvc.perform(get("/user/getStudentById/" + student.getStudentId().toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void deleteStudentById() {
        doNothing().when(delegate).deleteStudentById(student.getStudentId());
        mvc.perform(get("/user/deleteStudentById/" + student.getStudentId().toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}