package ru.edu.project.backend.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import ru.edu.project.backend.api.students.Student;
import ru.edu.project.backend.da.StudentDALayer;
import ru.edu.project.backend.da.jpa.JPAStudentDA;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

public class StudentServiceLayerTest {

    @InjectMocks
    private StudentServiceLayer serviceLayer;

    @Mock
    private StudentDALayer daLayer;

    @Spy
    private Student student;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void getAllStudents() {
        when(daLayer.getAllStudents()).thenReturn(Arrays.asList(student));
        List<Student> actual = serviceLayer.getAllStudents();

        assertEquals(1, actual.size());
        verify(daLayer,timeout(1)).getAllStudents();
    }

    @Test
    public void saveStudent() {
        when(daLayer.saveStudent(any(Student.class))).thenReturn(student);

        Student actual = serviceLayer.saveStudent(student);
        assertEquals(student, actual);
        verify(daLayer,timeout(1)).saveStudent(any(Student.class));
    }

    @Test
    public void getStudentById() {
        when(daLayer.getStudentById(any(Long.class))).thenReturn(student);

        Student actual = serviceLayer.getStudentById(1L);
        assertEquals(student, actual);
        verify(daLayer,timeout(1)).getStudentById(any(Long.class));
    }

    @Test
    public void deleteStudentById() {
        doNothing().when(daLayer).deleteStudentById(isA(Long.class));
        serviceLayer.deleteStudentById(1L);

        verify(daLayer,timeout(1)).deleteStudentById(any(Long.class));
    }
}