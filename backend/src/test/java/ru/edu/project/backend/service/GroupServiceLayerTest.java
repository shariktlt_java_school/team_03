package ru.edu.project.backend.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import ru.edu.project.backend.api.group.Group;
import ru.edu.project.backend.api.students.Student;
import ru.edu.project.backend.da.GroupDaLayer;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

public class GroupServiceLayerTest {

    @InjectMocks
    private GroupServiceLayer serviceLayer;

    @Mock
    private GroupDaLayer daLayer;

    @Spy
    private Group group;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void getByIdGroup() {
        when(daLayer.getByIdGroup(any(Long.class))).thenReturn(group);

        Group actual = serviceLayer.getByIdGroup(1L);
        assertEquals(group, actual);
        verify(daLayer,timeout(1)).getByIdGroup(any(Long.class));
    }

    @Test
    public void getStudentsByGroup() {
        Student student = mock(Student.class);
        when(daLayer.getStudentsByGroup(any(Long.class))).thenReturn(Arrays.asList(student));

        List<Student> actual = serviceLayer.getStudentsByGroup(1L);
        assertEquals(1,actual.size());
        verify(daLayer,timeout(1)).getStudentsByGroup(any(Long.class));
    }

    @Test
    public void createGroup() {
        String create = "Create Group";
        when(daLayer.createGroup(create)).thenReturn(group);

        Group actual = serviceLayer.createGroup(create);
        assertEquals(group, actual);
        verify(daLayer,timeout(1)).createGroup(any(String.class));
    }

    @Test
    public void removeGroup() {
        doNothing().when(daLayer).removeGroup(isA(Long.class));
        serviceLayer.removeGroup(1L);

        verify(daLayer,timeout(1)).removeGroup(any(Long.class));
    }

    @Test
    public void getAllGroup() {
        when(daLayer.getAllGroup()).thenReturn(Arrays.asList(group));
        List<Group> actual = serviceLayer.getAllGroup();

        assertEquals(1, actual.size());
        verify(daLayer,timeout(1)).getAllGroup();
    }
}