package ru.edu.project.backend.da.jpa;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import ru.edu.project.backend.api.group.Group;
import ru.edu.project.backend.api.students.Student;
import ru.edu.project.backend.converter.GroupConverter;
import ru.edu.project.backend.da.jpa.entity.GroupEntity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;


public class JPAGroupDaTest {

    @Mock
    private JPAGroupDa groupDa;

    @Mock
    private GroupConverter groupConverter;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void getByIdGroup() {
        Group group = Group.builder().id(1L).title("Title").studentList(new ArrayList<>()).build();
        when(groupDa.getByIdGroup(anyLong())).thenReturn(group);

        Group byIdGroup = groupDa.getByIdGroup(anyLong());
        assertNotNull(byIdGroup);
        verify(groupDa, timeout(1)).getByIdGroup(anyLong());
    }

    @Test
    public void createGroup() {
        Group group = Group.builder().id(1L).title("Title").studentList(new ArrayList<>()).build();
        when(groupConverter.buildGroup(any(GroupEntity.class))).thenReturn(group);
        when(groupDa.createGroup(anyString())).thenReturn(group);

        Group actual = groupDa.createGroup(anyString());
        assertNotNull(actual);
        verify(groupDa, timeout(1)).createGroup(anyString());
    }

    @Test
    public void removeGroup() {
        doNothing().when(groupDa).removeGroup(isA(Long.class));
        groupDa.removeGroup(anyLong());

        verify(groupDa, timeout(1)).removeGroup(anyLong());
    }

    @Test
    public void getStudentsByGroup() {
        Student student = Student.builder().studentId(1L).firstName("name").lastName("last").build();
        when(groupDa.getStudentsByGroup(anyLong())).thenReturn(Arrays.asList(student));

        List<Student> studentsByGroup = groupDa.getStudentsByGroup(anyLong());
        assertEquals(1, studentsByGroup.size());
        verify(groupDa, timeout(1)).getStudentsByGroup(anyLong());
    }

    @Test
    public void getAllGroup() {
        Group group = Group.builder().id(1L).title("Title").studentList(new ArrayList<>()).build();
        when(groupConverter.map(any(Iterable.class))).thenReturn(Arrays.asList(group));
        when(groupDa.getAllGroup()).thenReturn(Arrays.asList(group));

        List<Group> allGroup = groupDa.getAllGroup();
        assertEquals(1, allGroup.size());
        verify(groupDa, timeout(1)).getAllGroup();
    }
}