package ru.edu.project.backend.da.jdbctemplate;

import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import ru.edu.project.backend.api.group.Group;
import ru.edu.project.backend.api.students.Student;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.MockitoAnnotations.openMocks;
import static ru.edu.project.backend.da.jdbctemplate.StudentDA.SELECT_ALL_STUDENT;

public class StudentDATest {

    @InjectMocks
    private StudentDA studentDA;

    @Mock
    private JdbcTemplate jdbcTemplate;

    @Mock
    private NamedParameterJdbcTemplate jdbcNamed;

    @Spy
    private Student studentMock;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    @SneakyThrows
    public void getAllStudents() {
        ResultSet resultSetMock = Mockito.mock(ResultSet.class);
        Mockito.when(resultSetMock.getLong("student_id")).thenReturn(12L);
        Mockito.when(resultSetMock.getString("first_name")).thenReturn("firstName");
        Mockito.when(resultSetMock.getString("last_name")).thenReturn("lastName");
        Mockito.when(resultSetMock.getObject("group_id")).thenReturn(new Group());
        Mockito.when(resultSetMock.getFloat("progress")).thenReturn(5.02f);
        Mockito.when(resultSetMock.getBoolean("is_graduated")).thenReturn(false);
        Mockito.when(resultSetMock.getString("phone_number")).thenReturn("phoneNumber");
        Mockito.when(resultSetMock.getString("email")).thenReturn("email");

        List<Student> expectedResult = new ArrayList<>();
        Mockito.when(jdbcTemplate.query(eq(SELECT_ALL_STUDENT), any(RowMapper.class))).thenAnswer(invocationOnMock -> {
            RowMapper<Student> rowMapper = invocationOnMock.getArgument(1, RowMapper.class);
            expectedResult.add(rowMapper.mapRow(resultSetMock, 1));
            return expectedResult;
        });
        List<Student> allStudents = studentDA.getAllStudents();

        assertEquals(1, allStudents.size());
    }

    @Test
    public void saveStudent() {
        SimpleJdbcInsert jdbcInsert = Mockito.mock(SimpleJdbcInsert.class);
        studentMock.setStudentId(1L);
        Mockito.when(jdbcInsert.withTableName(any(String.class))).thenReturn(jdbcInsert);
        Mockito.when(jdbcInsert.usingGeneratedKeyColumns(any(String.class))).thenReturn(jdbcInsert);
        studentDA.setJdbcInsert(jdbcInsert);

        Mockito.when(jdbcInsert.executeAndReturnKey(any(Map.class))).thenReturn(1);
        Student saveStudent = studentDA.saveStudent(studentMock);
        assertEquals(1, saveStudent.getStudentId().longValue());

        Mockito.when(jdbcNamed.update(any(String.class), any(Map.class))).thenReturn(1);
        Student studentUpdate = studentDA.saveStudent(saveStudent);
        assertEquals(1, studentUpdate.getStudentId().longValue());
    }

    @Test
    @SneakyThrows
    public void getStudentById() {
        ResultSet resultSetMock = Mockito.mock(ResultSet.class);
        Mockito.when(resultSetMock.getLong("student_id")).thenReturn(12L);
        Mockito.when(resultSetMock.getString("first_name")).thenReturn("firstName");
        Mockito.when(resultSetMock.getString("last_name")).thenReturn("lastName");
        Mockito.when(resultSetMock.getObject("group_id")).thenReturn(any(Group.class));
        Mockito.when(resultSetMock.getFloat("progress")).thenReturn(5.02f);
        Mockito.when(resultSetMock.getBoolean("is_graduated")).thenReturn(false);
        Mockito.when(resultSetMock.getString("phone_number")).thenReturn("phoneNumber");
        Mockito.when(resultSetMock.getString("email")).thenReturn("email");

        Answer<Student> answer = new Answer<Student>() {
            public Student answer(InvocationOnMock invocationOnMock) throws Throwable {
                ResultSetExtractor<Student> extractor = invocationOnMock.getArgument(1);
                Student expected = extractor.extractData(resultSetMock);
                return expected;
            }
        };

        Mockito.when(jdbcTemplate.query(any(String.class), any(ResultSetExtractor.class), any(Long.class))).thenAnswer(answer);
        Student newStudent = studentDA.getStudentById(1L);
        assertNotNull(newStudent);
        assertEquals(12L, newStudent.getStudentId().longValue());
        assertEquals("firstName", newStudent.getFirstName());
        assertEquals("lastName", newStudent.getLastName());
        assertEquals("phoneNumber", newStudent.getPhoneNumber());
        assertEquals("email", newStudent.getEmail());
    }

    @Test
    public void deleteStudentById() {
        long id = 1;
        StudentDA studentDA = Mockito.mock(StudentDA.class);
        Mockito.doNothing().when(studentDA).deleteStudentById(Mockito.isA(Long.class));
        studentDA.deleteStudentById(id);

        Mockito.verify(studentDA, Mockito.timeout(1)).deleteStudentById(id);
    }
}