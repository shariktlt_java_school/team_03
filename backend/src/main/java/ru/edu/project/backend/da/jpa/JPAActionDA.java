package ru.edu.project.backend.da.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.action.Action;
import ru.edu.project.backend.da.ActionDALayer;
import ru.edu.project.backend.da.jpa.converter.ActionMapper;
import ru.edu.project.backend.da.jpa.entity.ActionEntity;
import ru.edu.project.backend.da.jpa.repository.ActionEntityRepository;

import java.util.List;

@Service
@Profile("SPRING_DATA")
public class JPAActionDA implements ActionDALayer {


    /**
     * Зависимость на репозиторий.
     */
    @Autowired
    private ActionEntityRepository repository;

    /**
     * Зависимость на маппер.
     */
    @Autowired
    private ActionMapper mapper;

    /**
     * Поиск действий по выполненному заданию.
     *
     * @param taskId
     * @return list
     */
    @Override
    public List<Action> findByTask(final long taskId) {
        return mapper.map(repository.findAllByPkTaskId(taskId));
    }

    /**
     * Сохранение действия.
     *
     * @param taskId
     * @param action
     * @return Action
     */
    @Override
    public Action save(final long taskId, final Action action) {
        ActionEntity draft = mapper.map(action);

        draft.getPk().setTaskId(taskId);

        return mapper.map(repository.save(draft));
    }
}
