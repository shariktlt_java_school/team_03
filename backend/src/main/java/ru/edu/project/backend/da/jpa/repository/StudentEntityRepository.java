package ru.edu.project.backend.da.jpa.repository;

import org.springframework.data.repository.CrudRepository;
import ru.edu.project.backend.da.jpa.entity.StudentEntity;

public interface StudentEntityRepository extends CrudRepository<StudentEntity, Long> {
}
