package ru.edu.project.backend.da.jpa.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.project.backend.da.jpa.entity.LessonLinkEntity;

import java.util.List;

@Repository
public interface LessonLinkEntityRepository extends CrudRepository<LessonLinkEntity, LessonLinkEntity.LessonLinkId> {

    /**
     * Поиск записей по полю составного ключа pk.TaskId.
     *
     * @param taskId
     * @return list
     */
    List<LessonLinkEntity> findAllByPkTaskId(long taskId);
}
