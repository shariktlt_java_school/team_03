package ru.edu.project.backend.da.jpa;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.students.Student;
import ru.edu.project.backend.converter.StudentConverter;
import ru.edu.project.backend.da.StudentDALayer;
import ru.edu.project.backend.da.jpa.entity.StudentEntity;
import ru.edu.project.backend.da.jpa.repository.StudentEntityRepository;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@Profile("SPRING_DATA")
public class JPAStudentDA implements StudentDALayer {

    /**
     * Зависимость на репозиторий.
     */
    @Autowired
    private StudentEntityRepository studRepo;

    /**
     * Конвертер.
     */
    @Autowired
    private StudentConverter converter;

    /**
     * Возвращает список студентов.
     *
     * @return List
     */
    @Override
    public List<Student> getAllStudents() {
        return converter.map(studRepo.findAll());
    }

    /**
     * Сохранение студента.
     *
     * @param student
     * @return Student
     */
    @Override
    public Student saveStudent(final Student student) {
        if (student == null) {
            log.info("Object Student is null");
        }

        StudentEntity save = studRepo.save(converter.buildEntityStudent(student));
        return converter.buildStudent(save);
    }

    /**
     * Возвращает студента по id.
     *
     * @param id
     * @return Student
     */
    @Override
    public Student getStudentById(final Long id) {
        Optional<StudentEntity> studentById = studRepo.findById(id);
        StudentEntity entity = studentById.get();
        return converter.buildStudent(entity);
    }

    /**
     * Удаление студента по id.
     *
     * @param id
     */
    @Override
    public void deleteStudentById(final Long id) {
        studRepo.deleteById(id);
    }
}
