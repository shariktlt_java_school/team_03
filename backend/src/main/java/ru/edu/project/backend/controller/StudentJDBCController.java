package ru.edu.project.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.project.backend.api.students.Student;
import ru.edu.project.backend.api.students.StudentService;
import ru.edu.project.backend.service.StudentServiceLayer;

import java.util.List;


@RestController
@RequestMapping("/user")
public class StudentJDBCController implements StudentService {

    /**
     * Делегат.
     */
    @Autowired
    private StudentServiceLayer delegate;

    /**
     * Возвращает список студентов.
     * @return List
     */
    @Override
    @GetMapping("/getAllStudents")
    public List<Student> getAllStudents() {
        return delegate.getAllStudents();
    }

    /**
     * Сохраняет студента.
     * @param student
     * @return Student
     */
    @Override
    @PostMapping("/saveStudent")
    public Student saveStudent(@RequestBody final Student student) {
        return delegate.saveStudent(student);
    }


    /**
     * Возвращает студента по id.
     * @param id
     * @return Student
     */
    @Override
    @GetMapping("/getStudentById/{id}")
    public Student getStudentById(@PathVariable("id") final Long id) {
        return delegate.getStudentById(id);
    }

    /**
     * Удаляет студента по id.
     * @param id
     */
    @Override
    @GetMapping("/deleteStudentById/{id}")
    public void deleteStudentById(@PathVariable("id") final Long id) {
        delegate.deleteStudentById(id);
    }
}
