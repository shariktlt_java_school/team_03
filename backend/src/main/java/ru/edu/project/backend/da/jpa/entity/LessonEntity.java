package ru.edu.project.backend.da.jpa.entity;

import lombok.Getter;
import lombok.Setter;
import ru.edu.project.backend.api.lessons.LessonAbstract;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "LESSON")
public class LessonEntity implements LessonAbstract {

    /**
     * Primary key- id занятия.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "lesson_seq")
    @SequenceGenerator(name = "lesson_seq",
            sequenceName = "lesson_id_sequence", allocationSize = 1)
    private Long id;

    /**
     * Название занятия.
     */
    private String title;

    /**
     * Содержание занятия.
     */
    private String description;

    /**
     * Задание к занятию.
     */
    private String issue;

    /**
     * Доступность занятий.
     */
    private Boolean enabled;
}
