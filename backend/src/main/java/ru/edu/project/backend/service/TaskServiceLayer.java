package ru.edu.project.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.action.CreateActionTask;
import ru.edu.project.backend.api.action.SimpleAction;
import ru.edu.project.backend.api.common.PageView;
import ru.edu.project.backend.api.common.Score;
import ru.edu.project.backend.api.common.Search;
import ru.edu.project.backend.api.tasks.TaskForm;
import ru.edu.project.backend.api.tasks.TaskInfo;
import ru.edu.project.backend.api.tasks.TaskService;
import ru.edu.project.backend.api.tasks.UpdateScoreTask;
import ru.edu.project.backend.da.TaskDALayer;
import ru.edu.project.backend.model.ActionType;
import ru.edu.project.backend.model.TaskScore;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
@Profile("!STUB")
@Qualifier("TaskServiceLayer")
public class TaskServiceLayer implements TaskService {

    /**
     * Зависимость для слоя доступа к данным выполненного задания.
     */
    @Autowired
    private TaskDALayer daLayer;

    /**
     * Зависимость для сервиса занятий.
     */
    @Autowired
    private LessonServiceLayer lessonService;

    /**
     * Зависимость для сервиса действий.
     */
    @Autowired
    private ActionServiceLayer actionServiceLayer;

    /**
     * Получение выполненных заданий студента.
     *
     * @param id
     * @return list
     */
    @Override
    public List<TaskInfo> getTaskByStudent(final long id) {
        return daLayer.getStudentTasks(id);
    }

    /**
     * Получение детальной информации по выполненному заданию.
     *
     * @param studentId
     * @param taskId
     * @return info
     */
    @Override
    public TaskInfo getDetailedInfo(final long studentId, final long taskId) {
        TaskInfo taskInfo = getDetailedInfo(taskId);
        if (taskInfo.getStudentId() != studentId) {
            throw new IllegalArgumentException("task for student not found");
        }
        return taskInfo;
    }

    /**
     * Получение детальной информации по выполненному заданию.
     *
     * @param taskId
     * @return info
     */
    @Override
    public TaskInfo getDetailedInfo(final long taskId) {
        TaskInfo taskInfo = daLayer.getById(taskId);
        if (taskInfo == null) {
            throw new IllegalArgumentException("task for student not found");
        }
        taskInfo.setServices(lessonService.getByLink(taskId));
        taskInfo.setActionHistory(actionServiceLayer.searchByTask(taskId));

        return taskInfo;
    }

    /**
     * Регистрация нового выполненного задания.
     *
     * @param taskForm
     * @return запись
     */
    @Override
    public TaskInfo createTask(final TaskForm taskForm) {

        Timestamp createdAt = new Timestamp(new Date().getTime());
        TaskInfo draft = TaskInfo.builder()
                .studentId(taskForm.getStudentId())
                .taskContent(taskForm.getTaskContent())
                .comment(taskForm.getComment())
                .createdAt(createdAt)
                .lastActionAt(createdAt)
                .score(TaskScore.WAIT_CHECK)
                .build();

        daLayer.save(draft);

        lessonService.link(draft.getId(), taskForm.getSelectedLessons());
        draft.setServices(lessonService.getByIds(taskForm.getSelectedLessons()));

        actionServiceLayer.createAction(CreateActionTask.builder()
                .taskId(draft.getId())
                .action(SimpleAction.builder()
                        .typeCode(ActionType.CREATED.getTypeCode())
                        .build())
                .build());

        draft.setActionHistory(actionServiceLayer.searchByTask(draft.getId()));

        return draft;
    }

    /**
     * Поиск выполненных заданий.
     *
     * @param search
     * @return list
     */
    @Override
    public PageView<TaskInfo> searchTasks(final Search search) {
        return daLayer.searchTask(search);
    }

    /**
     * Выставление оценки.
     *
     * @param updateScoreTask
     * @return boolean
     */
    @Override
    public boolean updateScore(final UpdateScoreTask updateScoreTask) {
        TaskInfo taskInf = daLayer.getById(updateScoreTask.getTaskId());

        Score score = TaskScore.byScoreCode(updateScoreTask.getScore().getScoreCode());

        if (taskInf == null || score == null) {
            return false;
        }
        Long oldScoreCode = taskInf.getScore().getScoreCode();

        taskInf.setScore(score);
        Timestamp timestamp = new Timestamp(new Date().getTime());
        taskInf.setLastActionAt(timestamp);
        daLayer.save(taskInf);

        actionServiceLayer.createAction(CreateActionTask.builder()
                .taskId(taskInf.getId())
                .action(SimpleAction.builder()
                        .typeCode(ActionType.CHECKED.getTypeCode())
                        .time(timestamp)
                        .message(oldScoreCode + " > " + score.getScoreCode())
                        .build())
                .build());
        return true;
    }
}
