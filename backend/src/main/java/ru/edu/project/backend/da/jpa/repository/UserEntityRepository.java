package ru.edu.project.backend.da.jpa.repository;

import org.springframework.data.repository.CrudRepository;
import ru.edu.project.backend.da.jpa.entity.UserEntity;

public interface UserEntityRepository extends CrudRepository<UserEntity, Long> {

    /**
     * Поиск записей.
     *
     * @param username
     * @return userEntity
     */
    UserEntity findByUsername(String username);
}
