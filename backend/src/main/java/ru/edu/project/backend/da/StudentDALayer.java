package ru.edu.project.backend.da;

import ru.edu.project.backend.api.students.Student;

import java.util.List;

public interface StudentDALayer {

    /**
     * Вернуть список всех студентов.
     *
     * @return List
     */
    List<Student> getAllStudents();

    /**
     * Сохранить студента.
     *
     * @param student
     * @return Student
     */
    Student saveStudent(Student student);

    /**
     * Вернуть студента по указанному ID.
     *
     * @param id
     * @return Student
     */
    Student getStudentById(Long id);

    /**
     * Удалить студента по ID.
     *
     * @param id
     */
    void deleteStudentById(Long id);
}
