package ru.edu.project.backend.da.jpa.converter;

import org.mapstruct.Mapper;
import ru.edu.project.backend.api.lessons.Lesson;
import ru.edu.project.backend.da.jpa.entity.LessonEntity;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LessonMapper {

    /**
     * Маппинг LessonEntity -> Lesson.
     *
     * @param entity
     * @return lesson
     */
    Lesson map(LessonEntity entity);


    /**
     * Маппинг List<LessonEntity> -> List<Lesson>.
     *
     * @param ids
     * @return list
     */
    List<Lesson> map(Iterable<LessonEntity> ids);

}
