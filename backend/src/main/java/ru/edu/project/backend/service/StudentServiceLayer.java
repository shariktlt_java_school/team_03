package ru.edu.project.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.students.Student;
import ru.edu.project.backend.api.students.StudentService;
import ru.edu.project.backend.da.StudentDALayer;

import java.util.List;

@Service
@Profile("!STUB")
@Qualifier("StudentServiceLayer")
public class StudentServiceLayer implements StudentService {

    /**
     * Зависимость для слоя доступа к данным.
     */
    @Autowired
    private StudentDALayer delegate;

    /**
     * Вернуть список студентов.
     *
     * @return список
     */
    @Override
    public List<Student> getAllStudents() {
        return delegate.getAllStudents();
    }

    /**
     * Сохранить студента.
     *
     * @param student
     * @return Student
     */
    @Override
    public Student saveStudent(final Student student) {
        return delegate.saveStudent(student);
    }

    /**
     * Вернуть студента по ID.
     *
     * @param id
     * @return
     */
    @Override
    public Student getStudentById(final Long id) {
        return delegate.getStudentById(id);
    }

    /**
     * Удалить студента по ID.
     *
     * @param id
     */
    @Override
    public void deleteStudentById(final Long id) {
        delegate.deleteStudentById(id);
    }
}
