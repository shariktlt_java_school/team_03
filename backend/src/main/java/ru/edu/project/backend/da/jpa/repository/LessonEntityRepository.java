package ru.edu.project.backend.da.jpa.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.project.backend.da.jpa.entity.LessonEntity;

import java.util.List;

@Repository
public interface LessonEntityRepository extends CrudRepository<LessonEntity, Long> {

    /**
     * Поиск записей по полю enabled.
     * @param enabled
     * @return list
     */
    List<LessonEntity> findAllByEnabled(boolean enabled);
}
