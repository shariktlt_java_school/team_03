package ru.edu.project.backend.converter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.edu.project.backend.api.group.Group;
import ru.edu.project.backend.api.students.Student;
import ru.edu.project.backend.da.jpa.entity.GroupEntity;
import ru.edu.project.backend.da.jpa.entity.StudentEntity;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Component
public class StudentConverter {

    /**
     * Вспомогательный метод по возврату списка.
     * Iterable<StudentEntity> -> List<Student>.
     * @param all
     * @return List
     */
    public List<Student> map(final Iterable<StudentEntity> all) {
        if (all == null) {
            log.info("Collection StudentEntity is null");
        }

        return StreamSupport.stream(all.spliterator(), false)
                .map(this::buildStudent)
                .collect(Collectors.toList());
    }

    /**
     * Формирует студента.
     * StudentEntity -> Student.
     * @param student
     * @return Student
     */
    public Student buildStudent(final StudentEntity student) {
        return Student.builder()
                .studentId(student.getStudentId())
                .firstName(student.getFirstName())
                .lastName(student.getLastName())
                .group(Group.builder()
                        .id(student.getGroup().getGroupId())
                        .title(student.getGroup().getTitle())
                        .build())
                .progress(student.getProgress())
                .isGraduated(student.getIsGraduated())
                .phoneNumber(student.getPhoneNumber())
                .email(student.getEmail())
                .build();
    }

    /**
     * Вспомогательный метод.
     * Student -> StudentEntity.
     * @param student
     * @return StudentEntity
     */
    public StudentEntity buildEntityStudent(final Student student) {
        return StudentEntity.builder()
                .studentId(student.getStudentId())
                .firstName(student.getFirstName())
                .lastName(student.getLastName())
                .group(GroupEntity.builder()
                        .groupId(student.getGroup().getId())
                        .title(student.getGroup().getTitle())
                        .build())
                .progress(student.getProgress())
                .isGraduated(student.getIsGraduated())
                .phoneNumber(student.getPhoneNumber())
                .email(student.getEmail())
                .build();
    }

    /**
     * Вспомогательный мектод.
     * Optional<StudentEntity> studEntity -> StudentEntity.
     * @param studEntity
     * @return StudentEntity
     */
    public StudentEntity getStudentEntityById(final Optional<StudentEntity> studEntity) {
        if (!studEntity.isPresent()) {
            log.info("Student not found by studentId");
        }
        return studEntity.get();
    }
}
