package ru.edu.project.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.project.backend.api.group.Group;
import ru.edu.project.backend.api.group.GroupService;
import ru.edu.project.backend.api.students.Student;
import ru.edu.project.backend.service.GroupServiceLayer;

import java.util.List;

@RestController
@RequestMapping("/group")
public class JpaGroupController implements GroupService {

    /**
     * Делегат.
     */
    @Autowired
    private GroupServiceLayer delegate;

    /**
     * Возвращает группу по Id.
     *
     * @param id
     * @return Group
     */
    @Override
    @GetMapping("/getByIdGroup/{id}")
    public Group getByIdGroup(@PathVariable final Long id) {
        return delegate.getByIdGroup(id);
    }

    /**
     * Создает новую группу.
     *
     * @param name
     * @return Group
     */
    @Override
    @PostMapping("/createGroup")
    public Group createGroup(@RequestBody final String name) {
        return delegate.createGroup(name);
    }

    /**
     * Удалить группу.
     *
     * @param id
     */
    @Override
    @GetMapping("/removeGroup/{id}")
    public void removeGroup(@PathVariable final Long id) {
        delegate.removeGroup(id);
    }

    /**
     * Возвращает список студентов одной группы.
     *
     * @param id
     * @return List
     */
    @Override
    @GetMapping("/getStudentsByGroup/{id}")
    public List<Student> getStudentsByGroup(@PathVariable final Long id) {
        return delegate.getStudentsByGroup(id);
    }

    /**
     * Вернуть все группы.
     *
     * @return List
     */
    @Override
    @GetMapping("/getAllGroup")
    public List<Group> getAllGroup() {
        return delegate.getAllGroup();
    }
}
