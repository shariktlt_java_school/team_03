package ru.edu.project.backend.da.jpa.converter;

import org.mapstruct.Mapper;
import ru.edu.project.backend.api.tasks.TaskInfo;
import ru.edu.project.backend.da.jpa.entity.TaskEntity;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TaskInfoMapper {

    /**
     * Маппинг TaskInfo -> TaskEntity.
     *
     * @param taskInfo
     * @return entity
     */
    TaskEntity map(TaskInfo taskInfo);

    /**
     * Маппинг TaskEntity -> TaskInfo.
     *
     * @param entity
     * @return info
     */
    TaskInfo map(TaskEntity entity);

    /**
     * Маппинг List<TaskEntity> -> List<TaskInfo>.
     *
     * @param listEntity
     * @return list
     */
    List<TaskInfo> mapList(List<TaskEntity> listEntity);
}
