package ru.edu.project.backend.da;

import ru.edu.project.backend.api.lessons.Lesson;

import java.util.List;

public interface LessonDALayer {

    /**
     * Связывание выполненного задания с занятием.
     *
     * @param taskId
     * @param lessonId
     */
    void linkTask(long taskId, long lessonId);

    /**
     * Получение занятия по id.
     *
     * @param id
     * @return lesson
     */
    Lesson getById(Long id);

    /**
     * Получение списка занятий по ids.
     *
     * @param ids
     * @return list
     */
    List<Lesson> getByIds(List<Long> ids);

    /**
     * Получение списка доступных занятий.
     *
     * @return list
     */
    List<Lesson> getAvailable();

    /**
     * Получение выбранного списка занятий по taskId.
     *
     * @param taskId
     * @return list
     */
    List<Lesson> getLinksByTaskId(long taskId);

}
