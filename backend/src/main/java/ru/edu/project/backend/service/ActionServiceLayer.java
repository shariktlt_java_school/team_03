package ru.edu.project.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.action.Action;
import ru.edu.project.backend.api.action.ActionService;
import ru.edu.project.backend.api.action.CreateActionTask;
import ru.edu.project.backend.api.action.SimpleAction;
import ru.edu.project.backend.da.ActionDALayer;
import ru.edu.project.backend.model.ActionType;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
@Profile("!STUB")
@Qualifier("ActionServiceLayer")
public class ActionServiceLayer implements ActionService {

    /**
     * Зависимость на слой доступа к данным.
     */
    @Autowired
    private ActionDALayer actionDALayer;

    /**
     * Поиск действий по выполненному заданию.
     *
     * @param taskId
     * @return list
     */
    @Override
    public List<Action> searchByTask(final long taskId) {
        return actionDALayer.findByTask(taskId);
    }

    /**
     * Создание действия для выполненного задания.
     *
     * @param createActionTask
     * @return action
     */
    @Override
    public Action createAction(final CreateActionTask createActionTask) {
        ActionType actionType = ActionType.byCode(createActionTask.getAction().getTypeCode());
        if (actionType == null) {
            throw new IllegalArgumentException("invalid action code "
                    + createActionTask.getAction().getTypeCode());
        }

        Timestamp timestamp = createActionTask.getAction().getTime();
        if (timestamp == null) {
            timestamp = new Timestamp(new Date().getTime());
        }

        return actionDALayer.save(
                createActionTask.getTaskId(),
                SimpleAction.builder()
                        .typeCode(actionType.getTypeCode())
                        .time(timestamp)
                        .message(createActionTask.getAction().getMessage())
                        .build());
    }
}
