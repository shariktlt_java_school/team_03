package ru.edu.project.backend.da.jpa.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Table(name = "LESSON_LINK",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"lesson_id", "task_id"})
        }
)
public class LessonLinkEntity {

    /**
     * Вынесение композитного ключа в отдельную встраиваемую сущность.
     */
    @EmbeddedId
    private LessonLinkId pk;

    /**
     * Связь с таблицей LESSON через поле lesson_id.
     */
    @ManyToOne
    @MapsId("lesson_id")
    private LessonEntity lesson;


    /**
     * Встраиваемая сущность с полями, входящими в состав композитного ключа.
     */
    @Embeddable
    @Getter
    @Setter
    public static class LessonLinkId implements Serializable {

        /**
         * Составная часть ключа task_id.
         */
        @Column(name = "task_id")
        private Long taskId;

        /**
         * Составная часть ключа lesson_id.
         */
        @Column(name = "lesson_id")
        private Long lessonId;

    }

    /**
     * Билдер primary key.
     *
     * @param taskId
     * @param lessonId
     * @return pk
     */
    public static LessonLinkId pk(final long taskId, final long lessonId) {
        LessonLinkId id = new LessonLinkId();
        id.setTaskId(taskId);
        id.setLessonId(lessonId);
        return id;
    }
}
