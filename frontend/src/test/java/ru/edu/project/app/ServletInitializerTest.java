package ru.edu.project.app;

import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.builder.SpringApplicationBuilder;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.openMocks;

public class ServletInitializerTest {

    private ServletInitializer servletInitializer = new ServletInitializer();

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void configure() {
        SpringApplicationBuilder application = mock(SpringApplicationBuilder.class);
        servletInitializer.configure(application);
        verify(application).sources(DemoFrontendApplication.class);
    }
}
