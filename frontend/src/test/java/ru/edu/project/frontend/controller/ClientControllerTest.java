package ru.edu.project.frontend.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.project.authorization.UserDetailsId;
import ru.edu.project.backend.api.group.Group;
import ru.edu.project.backend.api.group.GroupService;
import ru.edu.project.backend.api.students.Student;
import ru.edu.project.backend.api.students.StudentService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

public class ClientControllerTest {

    @InjectMocks
    private ClientController clientController;

    @Mock
    private StudentService studentService;

    @Mock
    private GroupService groupService;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void index() {
        Student expected = new Student();
        Model modelMock = mock(Model.class);
        Authentication auth = mock(Authentication.class);
        UserDetailsId detailsId = mock(UserDetailsId.class);
        when(auth.getPrincipal()).thenReturn(detailsId);
        when(studentService.getStudentById(anyLong())).thenReturn(expected);

        String viewName = clientController.index(modelMock, auth);

        assertEquals("client/student/index", viewName);
        verify(modelMock).addAttribute("students", expected);
    }

    /*@Test
    public void view() {
        long studentId = 100L;

        Student studentExpected = Student.builder().build();
        when(studentService.getStudentById(studentId)).thenReturn(studentExpected);

        ModelAndView view = clientController.view(studentId);

        assertEquals("client/student/view", view.getViewName());
        assertEquals(studentExpected, view.getModel().get("students"));
    }*/

    @Test
    public void groupIndex() {
        List<Group> expectedGroup = new ArrayList<>();
        when(groupService.getAllGroup()).thenReturn(expectedGroup);

        Model modelMock = mock(Model.class);
        String view = clientController.groupIndex(modelMock);

        assertEquals("client/group/index_group", view);
        verify(modelMock).addAttribute("groups", expectedGroup);
    }

    @Test
    public void viewGroup() {
        long groupId = 100L;

        Group groupExpected = Group.builder().build();
        Mockito.when(groupService.getByIdGroup(groupId)).thenReturn(groupExpected);

        ModelAndView view = clientController.viewGroup(groupId);

        assertEquals("client/group/view_group", view.getViewName());
        assertEquals(groupExpected, view.getModel().get("groups"));
    }
}