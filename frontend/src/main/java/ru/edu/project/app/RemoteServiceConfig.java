package ru.edu.project.app;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.web.client.RestTemplate;
import ru.edu.project.backend.RestServiceInvocationHandler;
import ru.edu.project.backend.api.group.GroupService;
import ru.edu.project.backend.api.lessons.LessonService;
import ru.edu.project.backend.api.students.StudentService;
import ru.edu.project.backend.api.tasks.TaskService;
import ru.edu.project.backend.api.user.UserService;

import java.lang.reflect.Proxy;

@Configuration
@Profile("REST")
@SuppressWarnings("unchecked")
public class RemoteServiceConfig {

    /**
     * Создаем rest-прокси.
     *
     * @param handler
     * @return StudentService
     */
    @Bean
    public StudentService studentService(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/student");
        return getProxy(handler, StudentService.class);
    }

    /**
     * Создаем rest-прокси.
     *
     * @param handler
     * @return GroupService
     */
    @Bean
    public GroupService groupService(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/group");
        return getProxy(handler, GroupService.class);
    }

    /**
     * Создаем rest-прокси.
     *
     * @param handler
     * @return rest-proxy
     */
    @Bean
    public UserService userService(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/user");
        return getProxy(handler, UserService.class);
    }

    /**
     * Создаем rest-прокси.
     *
     * @return RestTemplate
     */
    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public RestTemplate restTemplateBean() {
        return new RestTemplate();
    }

    /**
     * Rest-прокси для сервиса TaskService.
     *
     * @param handler
     * @return rest-proxy
     */
    @Bean
    public TaskService taskServiceRest(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/task");
        return getProxy(handler, TaskService.class);
    }

    /**
     * Rest-прокси для сервиса LessonService.
     *
     * @param handler
     * @return rest-proxy
     */
    @Bean
    public LessonService lessonService(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/lesson");
        return getProxy(handler, LessonService.class);
    }

    /**
     * Proxy method.
     *
     * @param handler
     * @param tClass
     * @param <T>
     * @return T
     */
    private <T> T getProxy(final RestServiceInvocationHandler handler, final Class<T>... tClass) {
        return (T) Proxy.newProxyInstance(this.getClass().getClassLoader(), tClass, handler);
    }
}
