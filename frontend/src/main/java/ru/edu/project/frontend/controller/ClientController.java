package ru.edu.project.frontend.controller;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.project.authorization.UserDetailsId;
import ru.edu.project.backend.api.group.Group;
import ru.edu.project.backend.api.group.GroupService;
import ru.edu.project.backend.api.lessons.LessonService;
import ru.edu.project.backend.api.students.Student;
import ru.edu.project.backend.api.students.StudentService;
import ru.edu.project.backend.api.tasks.TaskForm;
import ru.edu.project.backend.api.tasks.TaskInfo;
import ru.edu.project.backend.api.tasks.TaskService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

@Slf4j
@Controller
@RequestMapping("/client")
public class ClientController {

    /**
     * Атрибут модели для хранения списка студентов.
     */
    public static final String STUDENT = "students";

    /**
     * Атрибут модели для хранения списка групп.
     */
    public static final String GROUP = "groups";

    /**
     * Атрибут модели для хранения списка ошибок.
     */
    public static final String FORM_ERROR_ATTR = "errorsList";

    /**
     * Атрибут модели для хранения списка доступных занятий.
     */
    public static final String LESSONS_ATTR = "lessons";

    /**
     * Атрибут модели для хранения списка выполненных заданий на проверку.
     */
    public static final String TASKS_ATTR = "tasks";

    /**
     * Атрибут BindingResult.
     */
    public static final String ERROR = "ERROR";

    /**
     * Внедрение зависимости StudentService.
     */
    @Autowired
    private StudentService studentService;

    /**
     * Внедрение зависимости StudentService.
     */
    @Autowired
    private GroupService groupService;

    /**
     * Внедрение зависимости TaskService.
     */
    @Autowired
    private TaskService taskService;

    /**
     * Ссылка на сервис занятий.
     */
    @Autowired
    private LessonService lessonService;

    /**
     * Начальная страница.
     *
     * @param model
     * @param auth
     * @return String
     */
    @GetMapping("/")
    public String index(final Model model, final Authentication auth) {
        long studentId = getUserId(auth);
        Student studentAuthentication = studentService.getStudentById(studentId);
        if (studentAuthentication == null) {
            log.info("Sorry, but there is no student with this id.");
            return "redirect:/client/student/index";
        }
        model.addAttribute(STUDENT, studentAuthentication);

        return "client/student/index";
    }

    /**
     * Показ карточки User по id.
     *
     * @param studentId
     * @return ModelAndView
     */
    @GetMapping("/view/{id}")
    public ModelAndView view(final @PathVariable("id") Long studentId) {
        ModelAndView model = new ModelAndView("client/student/view");

        Student student = studentService.getStudentById(studentId);
        if (student == null) {
            model.setStatus(HttpStatus.NOT_FOUND);
            model.setViewName("client/student/viewNotFound");
            return model;
        }

        model.addObject(STUDENT, student);
        return model;
    }

    /**
     * Начальная страница GroupService.
     *
     * @param model
     * @return String
     */
    @GetMapping("/group")
    public String groupIndex(final Model model) {
        model.addAttribute(GROUP, groupService.getAllGroup());
        return "client/group/index_group";
    }

    /**
     * Отображение Group.
     *
     * @param groupId
     * @return ModelAndView
     */
    @GetMapping("/group/view/{id}")
    public ModelAndView viewGroup(final @PathVariable("id") Long groupId) {
        ModelAndView model = new ModelAndView("client/group/view_group");
        Group group = groupService.getByIdGroup(groupId);
        if (group == null) {
            model.setStatus(HttpStatus.NOT_FOUND);
            model.setViewName("client/student/viewNotFound");
            return model;
        }

        model.addObject(GROUP, group);
        return model;
    }

    /**
     * Отображение выполненного задания студента.
     *
     * @param model
     * @param auth
     * @return view
     */
    @GetMapping("/task")
    public String indexTask(final Model model, final Authentication auth) {
        long studentId = getUserId(auth);

        model.addAttribute(
                TASKS_ATTR,
                taskService.getTaskByStudent(studentId)
        );
        return "client/task/index_task";
    }

    /**
     * Получаем studentId.
     *
     * @param auth
     * @return studentId
     */
    private long getUserId(final Authentication auth) {
        Object principal = auth.getPrincipal();

        if (principal instanceof UserDetailsId) {
            return ((UserDetailsId) principal).getUserId();
        }
        throw new IllegalStateException("invalid auth.getPrincipal() object type");
    }

    /**
     * Просмотр выполненного задания по id.
     *
     * @param taskId
     * @param auth
     * @return modelAndView
     */
    @GetMapping("/task/view/{id}")
    public ModelAndView view(final @PathVariable("id") Long taskId, final Authentication auth) {
        long studentId = getUserId(auth);

        ModelAndView model = new ModelAndView("client/task/view_task");

        TaskInfo detailedInfo = taskService.getDetailedInfo(studentId, taskId);
        if (detailedInfo == null) {
            model.setStatus(HttpStatus.NOT_FOUND);
            model.setViewName("/client/task/viewNotTask");
            return model;
        }

        model.addObject("record", detailedInfo);
        return model;
    }

    /**
     * Отображение формы для создания выполненного задания на проверку.
     *
     * @param model
     * @return modelAndView
     */
    @GetMapping("/task/create")
    public String createForm(final Model model) {
        model.addAttribute(LESSONS_ATTR, lessonService.getAvailable());
        return "client/task/create_task";
    }

    /**
     * Получаем форму с предварительной валидацией.
     *
     * @param form
     * @param bindingResult
     * @param model
     * @param auth
     * @return redirect url
     */
    @PostMapping("/task/create")
    public String createFormProcessing(
            @Valid
            @ModelAttribute final ClientController.CreateForm form,
            final BindingResult bindingResult,
            final Model model,
            final Authentication auth
    ) {
        log.info("START: createFormProcessing");
        if (bindingResult.hasErrors()) {
            model.addAttribute(FORM_ERROR_ATTR, bindingResult.getAllErrors());
            return createForm(model);
        }
        final long studentId = getUserId(auth);

        TaskInfo task = taskService.createTask(TaskForm.builder()
                .studentId(studentId)
                .selectedLessons(form.getLessons())
                .taskContent(form.getTaskContent())
                .comment(form.getComment())
                .build());

        log.info("FINISH: createFormProcessing");
        return "redirect:/client/task/view/" + task.getId();
    }

    @Getter
    @Setter
    public static class CreateForm {

        /**
         * Для парсинга даты.
         */
        private static final DateFormat FORMAT;

        static {
            FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
        }

        /**
         * Выбранные занятия.
         */
        @NotNull
        private List<Long> lessons;

        /**
         * Текст выполненного задания.
         */
        @NotNull
        private String taskContent;

        /**
         * Пояснение.
         */
        @NotNull
        private String comment;
    }
}
