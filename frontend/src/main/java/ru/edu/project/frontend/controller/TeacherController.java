package ru.edu.project.frontend.controller;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.project.backend.api.common.Score;
import ru.edu.project.backend.api.common.Search;
import ru.edu.project.backend.api.students.StudentService;
import ru.edu.project.backend.api.tasks.TaskService;
import ru.edu.project.backend.api.tasks.UpdateScoreTask;

import java.util.Arrays;
import java.util.Optional;

@Slf4j
@Controller
@RequestMapping("/teacher")
public class TeacherController {

    /**
     * Имя атрибута с выполненными заданиями.
     */
    public static final String TASKS_ATTR = "tasksPage";

    /**
     * Имя атрибута со списком полей сортировки.
     */
    public static final String SORT_FIELDS = "sortFields";

    /**
     * Имя атрибута с текущим полем сортировки.
     */
    public static final String SORT_BY_FIELD = "sortByField";

    /**
     * Имя атрибута с направлением.
     */
    public static final String IS_ASCENDING = "isAscending";

    /**
     * Имя атрибута задания.
     */
    public static final String RECORD_ATTR = "record";

    /**
     * Имя атрибута оценок.
     */
    public static final String SCORES_ATTR = "scores";

    /**
     * Зависимость на сервис выполненных заданий.
     */
    @Autowired
    private TaskService taskService;

    /**
     * Внедрение зависимости StudentService.
     */
    @Autowired
    private StudentService studentService;

    /**
     * Просмотр заявок.
     *
     * @param searchBy
     * @param isAscending
     * @param page
     * @param perPage
     * @param searchId
     * @return modelAndView
     */
    @GetMapping("/")
    public ModelAndView index(
            @RequestParam(name = "searchBy", required = false, defaultValue = "") final String searchBy,
            @RequestParam(name = "isAscending", required = false, defaultValue = "1") final boolean isAscending,
            @RequestParam(name = "page", required = false, defaultValue = "1") final int page,
            @RequestParam(name = "perPage", required = false, defaultValue = "10") final int perPage,
            @RequestParam(name = "id", required = false, defaultValue = "-1") final int searchId
    ) {

        if (searchId > 0) {
            return new ModelAndView("redirect:/teacher/view/" + searchId);
        }

        ModelAndView model = new ModelAndView("teacher/index");
        SearchFields searchByField = SearchFields.byString(searchBy);

        model.addObject(
                TASKS_ATTR,
                taskService.searchTasks(Search.by(searchByField.getField(), isAscending, page - 1, perPage))
        );

        model.addObject(SORT_FIELDS, SearchFields.values());
        model.addObject(SORT_BY_FIELD, searchByField);
        model.addObject(IS_ASCENDING, isAscending);

        return model;
    }

    /**
     * Просмотр выполненного задания.
     *
     * @param id
     * @return modelAndView
     */
    @GetMapping("/view/{id}")
    public ModelAndView view(@PathVariable("id") final Long id) {

        ModelAndView modelAndView = new ModelAndView("teacher/view");
        modelAndView.addObject(RECORD_ATTR, taskService.getDetailedInfo(id));
        modelAndView.addObject(SCORES_ATTR, TaskScore.values());

        return modelAndView;
    }


    /**
     * Выставление оценки.
     *
     * @param id
     * @param scoreCode
     * @return redirect
     */
    @GetMapping("/view/{id}/setScore")
    public String updateScore(@PathVariable("id") final long id, @RequestParam("score") final int scoreCode) {

        log.info("START: updateScore");
        TaskScore score = TaskScore.getByScoreCode(scoreCode);

        if (score == null) {
            return "redirect:/teacher/view/" + id + "?error=score_invalid";
        }

        boolean res = taskService.updateScore(UpdateScoreTask.builder()
                .taskId(id)
                .score(score)
                .build());

        log.info("FINISH: updateScore");
        return "redirect:/teacher/view/" + id + "?updatedScore=" + res;
    }

    @Getter
    @AllArgsConstructor
    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum TaskScore implements Score {

        /**
         * Оценка по умолчанию.
         */
        WAIT_CHECK(1L, " "),

        /**
         * Отрицательная оценка.
         */
        NOT_APPROVED(3L, "Незачет"),

        /**
         * Положительная оценка.
         */
        APPROVED(2L, "Зачет");

        /**
         * Код оценки.
         */
        private final Long scoreCode;

        /**
         * Значение.
         */
        private final String scoreValue;

        /**
         * Получаем enum по коду.
         *
         * @param scoreCode
         * @return enum or null
         */
        public static TaskScore getByScoreCode(final int scoreCode) {
            return Arrays.stream(values())
                    .filter(e -> e.getScoreCode() == scoreCode)
                    .findFirst().orElse(null);
        }
    }

    @AllArgsConstructor
    @Getter
    public enum SearchFields {

        /**
         * Поиск по ID.
         */
        ID("id", "id"),

        /**
         * Поиск по дате создания.
         */
        CREATED_AT("createdAt", "дата создания"),

        /**
         * Поиск по последнему обновлению.
         */
        LAST_ACTION_AT("lastActionAt", "последнее обновление");

        /**
         * Поиск по оценке.
         */
        private final String field;

        /**
         * Название поля.
         */
        private final String msg;

        /**
         * Поиск поля по строке из запроса.
         *
         * @param searchBy
         * @return enum
         */
        public static SearchFields byString(final String searchBy) {
            Optional<SearchFields> searchFields = Arrays
                    .stream(values())
                    .filter(e -> e.name().equals(searchBy))
                    .findFirst();

            return searchFields.orElse(CREATED_AT);
        }
    }
}
