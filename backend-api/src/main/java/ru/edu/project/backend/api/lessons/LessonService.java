package ru.edu.project.backend.api.lessons;

import ru.edu.project.backend.api.common.AcceptorArgument;

import java.util.List;

public interface LessonService {

    /**
     * Получение доступных занятий.
     *
     * @return список
     */
    List<Lesson> getAvailable();

    /**
     * Получение занятий по коду.
     *
     * @param ids
     * @return список
     */
    @AcceptorArgument
    List<Lesson> getByIds(List<Long> ids);
}
