package ru.edu.project.backend.stub.tasks;

import ru.edu.project.backend.api.action.SimpleAction;
import ru.edu.project.backend.api.common.PageView;
import ru.edu.project.backend.api.common.ScoreImpl;
import ru.edu.project.backend.api.common.Search;
import ru.edu.project.backend.api.lessons.Lesson;
import ru.edu.project.backend.api.lessons.LessonService;
import ru.edu.project.backend.api.tasks.TaskForm;
import ru.edu.project.backend.api.tasks.TaskInfo;
import ru.edu.project.backend.api.tasks.TaskService;
import ru.edu.project.backend.api.tasks.UpdateScoreTask;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Arrays.asList;

public class InMemoryStubTaskService implements TaskService {

    /**
     * Ссылка на сервис с занятиями.
     */
    private LessonService servicesService;

    /**
     * Локальное хранилище выполненных заданий в RAM.
     */
    private Map<Long, List<TaskInfo>> db = new ConcurrentHashMap<>();

    /**
     * Локальный счетчик выполненных заданий.
     */
    private AtomicLong idCount = new AtomicLong(0);

    /**
     * Конструктор с зависимостью.
     *
     * @param bean
     */
    public InMemoryStubTaskService(final LessonService bean) {
        servicesService = bean;
    }

    /**
     * Получение выполненного задания на проверку.
     *
     * @param id
     * @return list
     */
    @Override
    public List<TaskInfo> getTaskByStudent(final long id) {
        if (db.containsKey(id)) {
            return db.get(id);
        }
        return Collections.emptyList();
    }

    /**
     * Получение детальной информации по выполненному студентом заданию.
     *
     * @param studentId
     * @param taskId
     * @return
     */
        @Override
        public TaskInfo getDetailedInfo(
                final long studentId,
                final long taskId
        ) {
            if (db.containsKey(studentId)) {
                Optional<TaskInfo> info = db.get(studentId).stream()
                        .filter(taskInfo -> taskId == taskInfo.getId())
                        .findFirst();
                return info.orElse(null);
            }
            return null;
        }

    /**
     * Получение детальной информации по выполненному заданию.
     *
     * @param taskId
     * @return
     */
    @Override
    public TaskInfo getDetailedInfo(final long taskId) {
        return db.values().stream().flatMap(Collection::stream)
                .filter(taskInfo -> taskId == taskInfo.getId())
                .findFirst().orElse(null);
    }

    /**
     * Регистрация нового выполненного задания.
     *
     * @param taskForm
     * @return info
     */
    @Override
    public TaskInfo createTask(final TaskForm taskForm) {

        TaskInfo info = TaskInfo.builder()
                .id(idCount.addAndGet(1))
                .studentId(taskForm.getStudentId())
                .createdAt(new Timestamp(new Date().getTime()))
                .taskContent(taskForm.getTaskContent())
                .comment(taskForm.getComment())
                .score(ScoreImpl.builder()
                        .scoreCode(1L)
                        .scoreValue(" ")
                        .build())
                .services(getLessonsById(taskForm))
                .actionHistory(asList(SimpleAction.builder()
                        .time(new Timestamp(new Date().getTime()))
                        .typeCode(1L)
                        .typeMessage("Выполнение")
                        .message("Выполнено")
                        .build()))
                .build();

        if (!db.containsKey(taskForm.getStudentId())) {
            db.put(taskForm.getStudentId(), new ArrayList<>());
        }

        db.get(taskForm.getStudentId()).add(info);

        return info;
    }

    private List<Lesson> getLessonsById(final TaskForm taskForm) {
        return servicesService.getByIds(taskForm.getSelectedLessons());
    }

    /**
     * Метод для поиска выполненных заданий.
     *
     * @param search
     * @return list
     */
    @Override
    public PageView<TaskInfo> searchTasks(final Search search) {
        Stream<TaskInfo> searchStream = db
                .values()
                .stream()
                .flatMap(Collection::stream);
        if ("createdAt".equals(search.getSortBy())) {
            searchStream = searchStream
                    .sorted((r1, r2) -> (search.isAscendingSort() ? 1 : -1) * r1
                            .getCreatedAt()
                            .compareTo(r2.getCreatedAt()));
        }
        if (search.getPage() > 1) {
            searchStream = searchStream
                    .skip(search.getPerPage() * search.getPerPage() - 1);
        }
        return PageView.<TaskInfo>builder()
                .page(search.getPage())
                .perPage(search.getPerPage())
                .totalPages(Long.valueOf(idCount.get() / search.getPerPage()).intValue())//
                .total(idCount.get())
                .elements(searchStream
                                .limit(search.getPerPage())
                                .collect(Collectors.toList()))
                .build();
    }

    /**
     * Выставление оценки выполненному заданию.
     *
     * @param updateScoreTask
     * @return
     */
    @Override
    public boolean updateScore(final UpdateScoreTask updateScoreTask) {
        TaskInfo taskInfo = getDetailedInfo(updateScoreTask.getTaskId());
        if (taskInfo == null) {
            return false;
        }
        taskInfo.setScore(updateScoreTask.getScore());
        return true;
    }
}
