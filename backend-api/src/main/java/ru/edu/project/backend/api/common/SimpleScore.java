package ru.edu.project.backend.api.common;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SimpleScore implements Score {

    /**
     * Код оценки.
     */
    private Long scoreCode;

    /**
     * Строковое значение оценки.
     */
    private String scoreValue;


    /**
     * Пустой конструктор.
     */
    public SimpleScore() {
    }

    /**
     * Конструктор.
     *
     * @param strScore
     */
    public SimpleScore(final String strScore) {
        this.scoreValue = strScore;
    }
}
