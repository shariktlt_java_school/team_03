package ru.edu.project.backend.api.tasks;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;
import ru.edu.project.backend.api.common.Score;

@Getter
@Builder
@Jacksonized
public class UpdateScoreTask {

    /**
     * id выполненного задания.
     */
    private long taskId;

    /**
     * Оценка выполненного задания.
     */
    private Score score;
}
