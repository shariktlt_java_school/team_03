package ru.edu.project.backend.api.tasks;

import ru.edu.project.backend.api.common.AcceptorArgument;
import ru.edu.project.backend.api.common.PageView;
import ru.edu.project.backend.api.common.Search;

import java.util.List;

/**
 * Сервис по отправке на проверку выполненных студентом заданий.
 *
 */
public interface TaskService {

    /**
     * Получение выполненного студентом задания на проверку.
     *
     * @param id
     * @return list
     */
    List<TaskInfo> getTaskByStudent(long id);

    /**
     * Получение детальной информации по выполненному студентом заданию.
     *
     * @param studentId
     * @param taskId
     * @return info
     */
    TaskInfo getDetailedInfo(long studentId, long taskId);

    /**
     * Получение детальной информации по выполненному заданию.
     *
     * @param taskId
     * @return info
     */
    TaskInfo getDetailedInfo(long taskId);

    /**
     * Регистрация нового выполненного задания.
     *
     * @param taskForm
     * @return info
     */
    @AcceptorArgument
    TaskInfo createTask(TaskForm taskForm);

    /**
     * Поиск сданных работ.
     *
     * @param search
     * @return info
     */
    @AcceptorArgument
    PageView<TaskInfo> searchTasks(Search search);

    /**
     * Выставление оценки выполненному задания.
     *
     * @param updateScoreTask
     * @return boolean
     */
    @AcceptorArgument
    boolean updateScore(UpdateScoreTask updateScoreTask);
}
