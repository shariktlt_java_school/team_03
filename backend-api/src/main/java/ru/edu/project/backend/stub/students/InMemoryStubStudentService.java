package ru.edu.project.backend.stub.students;

import ru.edu.project.backend.api.group.Group;
import ru.edu.project.backend.api.students.Student;
import ru.edu.project.backend.api.students.StudentService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class InMemoryStubStudentService implements StudentService {

    /**
     * Временный КЕШ.
     */
    private Map<Long, Student> db = new ConcurrentHashMap<>();

    {
        for (StudentsEnum s : StudentsEnum.values()) {
            db.put(s.getStudent().getStudentId(), s.getStudent());
        }
    }

    /**
     * Возвращает список студентов.
     *
     * @return List
     */
    @Override
    public List<Student> getAllStudents() {
        return new ArrayList<>(db.values());
    }

    /**
     * Сохраняет студента.
     *
     * @param student
     * @return
     */
    @Override
    public Student saveStudent(final Student student) {
        return db.put(student.getStudentId(), student);
    }

    /**
     * Возвращает студента по указанному ID.
     *
     * @param id
     * @return
     */
    @Override
    public Student getStudentById(final Long id) {
        return db.get(id);
    }

    /**
     * Удаляет студента по указанному ID.
     *
     * @param id
     */
    @Override
    public void deleteStudentById(final Long id) {
        db.remove(id);
    }


    public enum StudentsEnum {

        /**
         * Заглушка.
         */
        STUDENT_1(1L, "Николай", "Тюльков", 6.25f, false, "+79563251254", "Nikola@mail.ru"),

        /**
         * Заглушка.
         */
        STUDENT_2(2L, "Александр", "Тюльков", 7.25f, false, "+79563251245", "Sanya@mail.ru"),

        /**
         * Заглушка.
         */
        STUDENT_3(3L, "Артур", "Грикшас", 8.00f, false, "+7908653742", "Adolf@mail.ru");


        /**
         * Обьект студента.
         */
        private final Student student;

        /**
         * Конструктор.
         *
         * @param id
         * @param name
         * @param lastName
         * @param progress
         * @param isGraduated
         * @param phone
         * @param eMail
         */
        StudentsEnum(final Long id, final String name, final String lastName,
                     final float progress, final boolean isGraduated,
                     final String phone, final String eMail) {
            student = Student.builder()
                    .studentId(id)
                    .firstName(name)
                    .lastName(lastName)
                    .group(Group.builder().build())
                    .progress(progress)
                    .isGraduated(isGraduated)
                    .phoneNumber(phone)
                    .email(eMail)
                    .build();

        }


        /**
         * Возвращает обьект студента.
         *
         * @return Student
         */
        public Student getStudent() {
            return student;
        }
    }
}
