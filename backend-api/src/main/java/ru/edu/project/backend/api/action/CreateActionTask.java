package ru.edu.project.backend.api.action;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Builder
@Jacksonized
public class CreateActionTask {

    /**
     * id выполненного задания.
     */
    private long taskId;

    /**
     * Действие.
     */
    private Action action;

}
