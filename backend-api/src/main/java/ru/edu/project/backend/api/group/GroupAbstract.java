package ru.edu.project.backend.api.group;

import ru.edu.project.backend.api.students.Student;

import java.util.List;

public interface GroupAbstract {

    /**
     * id.
     *
     * @return long
     */
    Long getId();

    /**
     * title.
     *
     * @return string
     */
    String getTitle();

    /**
     * desc.
     *
     * @return List
     */
    List<Student> getStudentList();
}
