package ru.edu.project.backend.api.group;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.jackson.Jacksonized;
import ru.edu.project.backend.api.students.Student;

import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Jacksonized
@Builder
public class Group {

    /**
     * ID.
     */
    private Long id;

    /**
     * Название группы.
     */
    private String title;

    /**
     * Список студенотов группы.
     */
    private List<Student> studentList;
}
