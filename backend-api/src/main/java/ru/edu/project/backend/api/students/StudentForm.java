package ru.edu.project.backend.api.students;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Setter
@Jacksonized
@NoArgsConstructor
public class StudentForm {

    /**
     * ID студента.
     */
    private Long id;

    /**
     * Имя студента.
     */
    @NonNull
    private String firstName;

    /**
     * Фамилия студента.
     */
    @NonNull
    private String lastName;

    /**
     * Номер группы в которой находится студент.
     */
    private Long group;

    /**
     * Номер телефона студента.
     */
    @NonNull
    private String phoneNumber;

    /**
     * Почта студента.
     */
    @NonNull
    private String email;
}
