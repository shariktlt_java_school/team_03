package ru.edu.project.backend.api.students;

import ru.edu.project.backend.api.common.AcceptorArgument;

import java.util.List;

public interface StudentService {

    /**
     * Возвращает список студентов.
     * @return List
     */
    List<Student> getAllStudents();

    /**
     * Сохранение студента.
     * @param student
     * @return Student
     */
    @AcceptorArgument
    Student saveStudent(Student student);

    /**
     * Возвращает студента по указанному ID.
     * @param id
     * @return Student
     */
    Student getStudentById(Long id);

    /**
     * Удаляет студента по указанному ID.
     * @param id
     */
    void deleteStudentById(Long id);
}
